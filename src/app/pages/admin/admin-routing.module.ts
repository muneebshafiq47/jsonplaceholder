import { AuthGuard } from './../../shared/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        redirectTo: 'posts',
        pathMatch: 'full'
      },
      // Allow acces to /posts if user has 'catalog.read' permissions
      {
        path: 'posts',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        data: { permission: 'catalog.read' },
        loadChildren: () =>
          import('./posts/posts.module').then((m) => m.PostsModule),
      },
      // Allow acces to /users if user has 'user.read' permissions
      {
        path: 'users',
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        data: { permissions: 'user.read' },
        loadChildren: () =>
          import('./users/users.module').then((m) => m.UsersModule),
      },
    ]
  },

  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule { }
